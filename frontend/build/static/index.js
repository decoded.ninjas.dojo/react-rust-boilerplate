const fetchMessage = async () => {

  //Get message from Rust backend
  const res = await fetch("/tester");
  const body = await res.json();
  const message = body.message;

  //Show message from backend
  const rootDiv = document.querySelector("#root");
  const h1 = document.createElement("h1");
  h1.textContent = message;
  rootDiv.appendChild(h1);

};

fetchMessage();


