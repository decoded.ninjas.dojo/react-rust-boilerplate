import {
  AppBar,
  Box,
  Button,
  Grid,
  Input,
  Toolbar,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Navigate,
  Route,
  Routes,
  useNavigate,
} from "react-router-dom";
import ts from "typescript";

interface ProtectedRouteProps {
  isLoggedIn: boolean;
  children: JSX.Element;
}

interface formDataObject {
  email?: String,
  password?: String
}

interface userObject {
  localId?: String,
  email?: String;
  displayName?: String;
}

const ProtectedRoute = ({
  isLoggedIn,
  children,
}: ProtectedRouteProps): JSX.Element => {
  if (!isLoggedIn) {
    return <Navigate to="/" replace />;
  }

  return children;
};

export const App = (): JSX.Element => {
  const userData: userObject = {};
  const emptyForm: formDataObject = {
    email: "",
    password: ""
  }
  const [user, setUser] = useState(userData);
  const [formData, setFormData] = useState(emptyForm);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const navigate = useNavigate()


  // Check if user logged in before via cookies
  useEffect(() => {

    // Helper function to extract cookie values
    const getCookieValue = cookieName => {
      let val = document.cookie
        .split('; ')
        .find(row => row.startsWith(`${cookieName}=`))
        ?.split('=')[1]

        if (val === undefined || val.length === 0){
          return "N/A"
        }

        return val
    }
    
    let hasPastLogin = true;
    const cookieNames = ["localId", "email", "displayName"]
    const cookiesMap: ts.ESMap<String,String> = new Map()

    // Email and LocalId should be populated to indicate valid cookies
    // Display Name can be null/undefined so we don't use it as a flag
    cookieNames.forEach( cookie => {
      let val = getCookieValue(cookie);
      if (val === "N/A" && cookie != "displayName"){
        hasPastLogin = false
      }
        
      cookiesMap.set(cookie, val)
    })


    // Use cookie data to populate state and redirect to authenticated page
    if (hasPastLogin){
      setUser({
        email: cookiesMap.get("email"),
        localId: cookiesMap.get("localId"),
        displayName: cookiesMap.get("displayName")
      })

      setIsLoggedIn(true)
      navigate("/authenticated")
    }

  }, [isLoggedIn])

  const handleInputChange = (e) => {

    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  // If login successful, response will include cookies with data
  // Use Effect watches for changes to IsLoggedIn state variable
  // Setting isLoggedIn to true triggers the app to parse cookies,
  // populate data, and redirect to authenticated page
  async function handleLogin(e){
    e.preventDefault()

    const body = {
      ...formData,
      returnSecureToken: true
    }

    let res = await fetch("/public/auth/sign-in",
    {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
    })

    if (res.status === 200){
      let user = await res.json()
      setFormData(emptyForm)
      // setUser(user);
      setIsLoggedIn(true)

//      navigate("/authenticated")

    }else {
      alert("Error logging in!") 
    }
  }

  // Logout is a GET request that response deletes cookies
  // IsLoggedIn value change trigger UseEffect but since coookies are deleted, does nothing
  const handleLogout = async (e) => {
    e.preventDefault()

    let res = await fetch("/private/auth/logout",
    {
      method: "GET"
    })

    if (res.status === 200){
      setUser({})
      setIsLoggedIn(false)
    }else {
      alert("Error logging out!")
    }

  }

  return (
    <Grid
      container
      spacing={2}
      sx={{
        height: "100vh",
      }}
    >
      <Grid item xs={12} md={12}>
        <AppBar position="static">
          <Toolbar
            sx={{
              bgcolor: "white",
              justifyContent: "end",
            }}
          >
            <Typography sx={{ color: "black" }}>
              {isLoggedIn ? user.email : "Guest"}
            </Typography>
          </Toolbar>
        </AppBar>
      </Grid>

        <Routes>
          <Route
            path="/"
            element={
              <Grid item xs={12}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    textAlign: "center",
                    flexDirection: "column",
                  }}
                >
                  <Typography variant="h3">Hello!</Typography>

                  <Input name="email" placeholder="Email" 
                    value={formData.email}
                    onChange={handleInputChange}/>
                  <Input name="password"
                    type="password" 
                    placeholder="Password"
                    value={formData.password}
                    onChange={handleInputChange}/>

                  <Box
                    sx={{
                      paddingTop: "2%",
                    }}
                  >
                    <Button variant="outlined" onClick={handleLogin}>Log in</Button>
                  </Box>
                </Box>
              </Grid>
            }
          />

          <Route
            path="/authenticated"
            element={
              <ProtectedRoute isLoggedIn={isLoggedIn}>
                <Grid item xs={12} sx={{
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  flexDirection: "column"
                }}>

                  <Typography variant="h3">You are logged in!</Typography>
                  <Button variant="outlined" onClick={handleLogout}>Logout</Button>

                </Grid>
              </ProtectedRoute>
            }
          />

          <Route
            path="*"
            element={<h1>NOT FOUND</h1>}>
          </Route>

        </Routes>
    </Grid>
  );
};
