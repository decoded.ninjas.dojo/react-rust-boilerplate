FROM node:18-buster AS frontend_build
WORKDIR /build
COPY /frontend/ frontend/
RUN cd frontend && yarn install && yarn run build 

FROM rust:1-buster
WORKDIR /app
ARG FIREBASE_API_KEY
ARG APP_ORIGIN
ENV FIREBASE_API_KEY=$FIREBASE_API_KEY
ENV APP_ORIGIN=$APP_ORIGIN
COPY Cargo.toml .
COPY /src/ src/
COPY --from=frontend_build /build/frontend/dist/ frontend/dist/
RUN cargo build --release

#Allow only non-root access
RUN useradd -m webuser
USER webuser

CMD ["/app/target/release/react-rust-boilerplate"]

EXPOSE 3000
