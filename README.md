# react-rust-boilerplate

A simple boilerplate for a React Frontend served from a Rust Backend powered by Axum.

## Getting started

1. Install Rust
  
    [Official Guide From Rust](https://www.rust-lang.org/tools/install)

2. Build/Run App

    ```bash
    git clone git@gitlab.com:decoded.ninjas.dojo/react-rust-boilerplate.git
    cd react-rust-boilerplate/frontend
    yarn install
    yarn run build
    cd ../
    source env.sh # Input Firebase API Key in bash file
    cargo build
    cargo run
    ```
3. View App

    Application by default runs on [localhost:3000](http://localhost:3000).

## Auth Endpoints

|Path | Payload | Response | Status Code |
|-----|---------|----------|--------|
|`/public/auth/sign-in`|`{"email": String, "password": String, "returnSecureToken": bool}`|`{"localId": String, "email": String, "displayName": String}`| 200 |
|`/public/auth/sign-up`|`{"email": String, "password": String, "returnSecureToken": bool}`|`{"localId": String, "email": String, "displayName": String}` | 200 |
|`/public/auth/sign-in-anon`|`{"returnSecureToken": bool}`| `{"localId": String, "email": String, "displayName": String}`| 200 |
|`/private/auth/logout`| `N/A` | `Successfully logged out.`| 200 |
|`/private/auth/delete-account`| `{"idToken": String}` | `Deleted.`| 200 |


## Tech Stack
- Frontend: [ReactJS](https://reactjs.org)
- Backend [Axum](https://github.com/tokio-rs/axum)
- Database: [Google Firebase](https://firebase.google.com/docs) 
