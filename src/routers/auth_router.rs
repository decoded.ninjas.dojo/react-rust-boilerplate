use crate::services::auth_service;
use axum::{
    routing::{get, post},
    Router,
};

pub fn public() -> Router {
    Router::new()
        .route("/", get(|| async { "Hello!" }))
        .route("/sign-in", post(auth_service::sign_in_user))
        .route("/sign-up", post(auth_service::sign_up_user))
        .route("/sign-in-anon", get(auth_service::sign_in_anon))
}

pub fn private() -> Router {
    Router::new()
        .route("/delete-account", post(auth_service::delete_user))
        .route("/token", post(auth_service::refresh_token))
        .route("/logout", get(auth_service::logout))
}
