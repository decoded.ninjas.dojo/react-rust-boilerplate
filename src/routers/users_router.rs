use crate::services::users_service;
use axum::{
    routing::{get, post},
    Router,
};

pub fn router() -> Router {
    Router::new()
        .route("/", get(|| async { "Hello!" }))
        .route("/", post(users_service::create_user))
}
