use crate::models::state::State;
use axum::extract::{FromRequest, RequestParts};
use axum::{middleware::Next, response::Response};
use hyper::{Request, StatusCode};
use tower_cookies::Cookies;

pub async fn auth<B: Send>(req: Request<B>, next: Next<B>) -> Result<Response, StatusCode> {
    let mut parts: RequestParts<B> = RequestParts::new(req);

    let cookies: Cookies = Cookies::from_request(&mut parts).as_mut().await.unwrap();

    let session_token: Option<String> = cookies
        .get("sessionToken")
        .and_then(|cookie| cookie.value().parse().ok());

    let refresh_token: Option<String> = cookies
        .get("sessionRefreshToken")
        .and_then(|cookie| cookie.value().parse().ok());

    if session_token.is_none() || refresh_token.is_none() {
        return Err(StatusCode::UNAUTHORIZED);
    }

    let req: Request<B> = parts.try_into_request().ok().unwrap();
    return Ok(next.run(req).await);
}
