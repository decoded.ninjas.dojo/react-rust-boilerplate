use axum::http::{HeaderValue, Method};
use std::env;
use tower::{
    layer::util::{Identity, Stack},
    ServiceBuilder,
};
use tower_http::cors::CorsLayer;

pub fn get_cors_svc() -> ServiceBuilder<Stack<CorsLayer, Identity>> {
    let origin: &'static str = env!("APP_ORIGIN");

    let cors_layer = CorsLayer::new()
        .allow_methods([Method::GET])
        .allow_origin(origin.parse::<HeaderValue>().unwrap());

    ServiceBuilder::new().layer(cors_layer)
}
