use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

#[derive(Debug)]
struct Session {
    local_id: String,
    access_token: String,
    refresh_token: String,
}

#[derive(Clone, Debug)]
pub struct State {
    db: Arc<RwLock<HashMap<String, Session>>>,
}

impl State {
    pub fn new() -> Self {
        State {
            db: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}
