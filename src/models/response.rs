use serde::{Deserialize, Serialize};

#[derive(Serialize)]
pub struct User {
    id: u64,
    username: String,
}

impl User {
    pub fn new(id: u64, username: String) -> User {
        User { id, username }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInResponse {
    local_id: String,
    email: String,
    display_name: String,
    #[serde(rename(serialize = "accessToken"), skip_serializing)]
    id_token: String,
    #[serde(skip_serializing)]
    refresh_token: String,
}

impl SignInResponse {

    pub fn local_id(&self) -> &str{
        &self.local_id
    }

    pub fn email(&self) -> &str {
        &self.email
    }

    pub fn display_name(&self) -> &str {
        &&self.display_name
    }

    pub fn access_token(&self) -> &str {
        &self.id_token
    }

    pub fn refresh_token(&self) -> &str {
        &self.refresh_token
    }
}

#[derive(Serialize)]
pub struct GenericResponse {
    pub message: String,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct RefreshTokenResponse {
    user_id: String,
    #[serde(rename(serialize = "accessToken"))]
    id_token: String,
    refresh_token: String,
}
