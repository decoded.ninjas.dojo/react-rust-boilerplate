use std::collections::HashMap;

use serde::{Deserialize, Serialize};

// the input to our `create_user` handler
#[derive(Deserialize)]
pub struct CreateUser {
    username: String,
}

impl CreateUser {
    pub fn get_username(&self) -> String {
        self.username.clone()
    }
}

#[derive(Deserialize, Serialize)]
pub enum Load {
    SignInPayload(SignInPayload), //   SignInPayload{email: String, password: String, returnSecureToken: bool}
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInPayload {
    email: String,
    password: String,
    return_secure_token: bool,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SignInAnonPayload {
    return_secure_token: bool,
}

impl SignInAnonPayload {
    pub fn new() -> Self {
        SignInAnonPayload {
            return_secure_token: true,
        }
    }
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteAccountPayload {
    id_token: String,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct RefreshTokenPayload {
    grant_type: String,
    refresh_token: String,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct FirestoreRequestPayload {
    fields: HashMap<String, HashMap<String, String>>,
}

impl FirestoreRequestPayload {
    pub fn new(obj: HashMap<String, HashMap<String, String>>) -> Self {
        FirestoreRequestPayload { fields: obj }
    }
}
