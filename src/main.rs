use axum::{
    http::{StatusCode, Uri},
    middleware as axum_middleware,
    response::{IntoResponse},
    routing::{get, get_service, MethodRouter},
    Json, Router,
};
use std::env;
use tower::{ServiceBuilder};
use tower_cookies::CookieManagerLayer;

use std::io;
use std::net;
use tower_http::{
    add_extension::AddExtensionLayer,
    services::{ServeDir, ServeFile},
};
mod firebase;
mod middleware;
mod models;
mod routers;
mod services;

use middleware::{cors, session};
use models::{response::GenericResponse, state::State};
use routers::{auth_router, users_router};

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    let port: String = env::var("PORT").unwrap_or("3000".to_string());
    let addr = net::SocketAddr::from(([0, 0, 0, 0], port.parse::<u16>().unwrap()));

    tracing::info!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app().into_make_service())
        .await
        .expect("Server Error!");
}

fn app() -> Router {
    let state: State = State::new();
    let static_svc: MethodRouter = get_service(
        ServeDir::new("frontend/dist").fallback(ServeFile::new("frontend/dist/index.html")),
    )
    .handle_error(|error: io::Error| async move {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Error: {}", error),
        )
    });

    let public_routes: Router = Router::new()
        //.route("/", get(index_html))
        .route(
            "/tester",
            get(|| async {
                let message: String = String::from("Hello from the backend!");
                (StatusCode::OK, Json(GenericResponse { message }))
            }),
        )
        .nest("/public/auth", auth_router::public());
    //.nest("/static", static_svc);

    let private_routes: Router = Router::new()
        .nest("/users", users_router::router())
        .nest("/private/auth", auth_router::private())
        .layer(
            ServiceBuilder::new()
                .layer(axum_middleware::from_fn(session::auth))
                .layer(AddExtensionLayer::new(state)),
        );

    Router::new()
        .merge(public_routes)
        .merge(private_routes)
        .layer(
            ServiceBuilder::new()
                .layer(cors::get_cors_svc().into_inner())
                .layer(CookieManagerLayer::new()),
        )
        .fallback(static_svc)
    //.fallback(fallback.into_service())
}


async fn handle_error(error: io::Error) -> impl IntoResponse {
    (
        StatusCode::INTERNAL_SERVER_ERROR,
        format!("Error: {}", error),
    )
}

async fn fallback(uri: Uri) -> impl IntoResponse {
    let message: String = format!("Invalid Route: '{}'", uri);
    (StatusCode::NOT_FOUND, Json(GenericResponse { message }))
}
