use axum::{http::StatusCode, response::IntoResponse, Json};
use tower_cookies::Cookies;

use crate::firebase::db;
use crate::firebase::errors::FirebaseError;
use crate::models::request::CreateUser;

pub async fn create_user(
    // this argument tells axum to parse the request body
    // as JSON into a `CreateUser` type
    //   Json(payload): Json<CreateUser>,
    // req: Request<Body>,
    cookies: Cookies,
    Json(payload): Json<CreateUser>,
) -> Result<impl IntoResponse, FirebaseError> {
    //   let user = User::new(1337, payload.username);
    // this will be converted into a JSON response
    // with a status code of `201 Created`

    let res = db::create_user(cookies, payload).await?;
    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;

    match status {
        &StatusCode::OK => {
            let obj = &body["fields"];
            Ok((StatusCode::CREATED, Json(obj.clone())))
        }
        other_status => {
            let error_message = body["error"]["message"].as_str().unwrap().to_string();

            let err = FirebaseError::new(other_status.clone(), error_message);
            Err(err)
        }
    }
}
