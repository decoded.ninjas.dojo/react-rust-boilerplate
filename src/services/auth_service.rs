use crate::firebase::{auth, errors::FirebaseError};
use crate::models::request::{
    DeleteAccountPayload, RefreshTokenPayload, SignInAnonPayload, SignInPayload,
};
use crate::models::response::{RefreshTokenResponse, SignInResponse};
use axum::{response::IntoResponse, Json};
use hyper::{Body, Response, StatusCode};
use tower_cookies::{Cookie, Cookies};
use cookie::SameSite;

async fn handle_sign_in_response(
    status: &StatusCode,
    body: serde_json::Value,
    cookie: Cookies,
) -> Result<impl IntoResponse, FirebaseError> {
    match status {
        &StatusCode::OK => {
            let body: SignInResponse = serde_json::from_value(body)?;

            let local_id: &str = body.local_id();
            let email: &str = body.email();
            let display_name: &str = body.display_name();
            let access_token: &str = body.access_token();
            let refresh_token: &str = body.refresh_token();

            let local_id_cookie: Cookie = build_public_cookie(String::from("localId"), String::from(local_id));
            let email_cookie: Cookie = build_public_cookie(String::from("email"), String::from(email));
            let display_name_cookie: Cookie = build_public_cookie(String::from("displayName"), String::from(display_name));
            let access_token_cookie: Cookie = build_cookie(String::from("sessionToken"), String::from(access_token));
            let refresh_token_cookie: Cookie = build_cookie(String::from("sessionRefreshToken"), String::from(refresh_token));

            cookie.add(local_id_cookie);
            cookie.add(email_cookie);
            cookie.add(display_name_cookie);
            cookie.add(access_token_cookie);
            cookie.add(refresh_token_cookie);

            return Ok((StatusCode::OK, Json(body)));
        }
        other_code => {
            let error_message = body["error"]["message"].as_str().unwrap().to_string();
            let err = FirebaseError::new(other_code.clone(), error_message);
            Err(err)
        }
    }
}

pub async fn sign_up_user(
    cookie: Cookies,
    Json(payload): Json<SignInPayload>,
) -> Result<impl IntoResponse, FirebaseError> {
    let res: Response<Body> = auth::create_user(&payload).await?;
    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;

    handle_sign_in_response(&status, body, cookie).await
}

pub async fn sign_in_user(
    cookie: Cookies,
    Json(payload): Json<SignInPayload>,
) -> Result<impl IntoResponse, FirebaseError> {
    let res: Response<Body> = auth::sign_in_user(&payload).await?;
    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;

    handle_sign_in_response(&status, body, cookie).await
}

pub async fn sign_in_anon(cookie: Cookies) -> Result<impl IntoResponse, FirebaseError> {
    let payload: Json<SignInAnonPayload> = Json(SignInAnonPayload::new());
    let res: Response<Body> = auth::sign_in_anon(&payload).await?;
    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;

    handle_sign_in_response(&status, body, cookie).await
}

pub async fn delete_user(Json(payload): Json<DeleteAccountPayload>) -> impl IntoResponse {
    let res: Response<Body> = auth::delete_user(&payload).await?;
    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;
    let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;

    match status {
        &StatusCode::OK => {
            return Ok((StatusCode::OK, "Deleted."));
        }
        other_status => {
            let error_message = body["error"]["message"].as_str().unwrap().to_string();

            let err = FirebaseError::new(other_status.clone(), error_message);
            Err(err)
        }
    }
}

pub async fn refresh_token(
    Json(payload): Json<RefreshTokenPayload>,
) -> Result<impl IntoResponse, FirebaseError> {
    let res: Response<Body> = auth::refresh_token(&payload).await?;

    let status = &res.status();
    let body_bytes = hyper::body::to_bytes(res.into_body()).await?;

    match status {
        &StatusCode::OK => {
            let json_res: RefreshTokenResponse = serde_json::from_slice(&body_bytes)?;
            return Ok((StatusCode::OK, Json(json_res)));
        }
        other_code => {
            let body: serde_json::Value = serde_json::from_slice(&body_bytes)?;
            let error_message = body["error"]["message"].as_str().unwrap().to_string();
            let err = FirebaseError::new(other_code.clone(), error_message);
            Err(err)
        }
    }
}

pub async fn logout(cookie: Cookies) -> impl IntoResponse {
    let local_id_cookie: Cookie = build_public_cookie(String::from("localId"), String::from(""));
    let email_cookie: Cookie = build_public_cookie(String::from("email"), String::from(""));
    let display_name_cookie: Cookie = build_public_cookie(String::from("displayName"), String::from(""));
    let access_token_cookie: Cookie = build_cookie(String::from("sessionToken"), String::from(""));
    let refresh_token_cookie: Cookie = build_cookie(String::from("sessionRefreshToken"), String::from(""));

    cookie.remove(local_id_cookie);
    cookie.remove(email_cookie);
    cookie.remove(display_name_cookie);
    cookie.remove(access_token_cookie);
    cookie.remove(refresh_token_cookie);
    
    (StatusCode::OK, "Successfully logged out.");
}

fn build_cookie<'a>(name:String, value: String) ->  Cookie<'a> {
    Cookie::build(name, value)
        .path("/")
        .http_only(true)
        .same_site(SameSite::Strict)
        .finish()
}


fn build_public_cookie<'a>(name:String, value: String) ->  Cookie<'a> {
    Cookie::build(name, value)
        .path("/")
        .same_site(SameSite::Strict)
        .finish()
}
