use std::collections::HashMap;

use hyper::{header, Body, Client, Method, Request, Response, Uri};
use hyper_tls::HttpsConnector;
use serde_json;
use tower_cookies::Cookies;

use crate::firebase::errors::FirebaseError;
use crate::models::request::{CreateUser, FirestoreRequestPayload};

type Result<T> = std::result::Result<T, FirebaseError>;

static DB_BASE_URI: &str = "https://firestore.googleapis.com/v1/projects/react-rust-boilerplate/databases/(default)/documents";

async fn post<T>(uri: &Uri, body: T, cookie: Cookies) -> Result<Response<Body>>
where
    T: serde::Serialize,
{
    let url: String = format!("{}{}", DB_BASE_URI, uri);

    let session_token: String = cookie
        .get("sessionToken")
        .and_then(|cookie| cookie.value().parse().ok())
        .unwrap();

    let _refresh_token: String = cookie
        .get("sessionRefreshToken")
        .and_then(|cookie| cookie.value().parse().ok())
        .unwrap();

    let auth_header_value = format!("Bearer {}", session_token);

    let str_body: String = serde_json::to_string(&body)?;
    let req = Request::builder()
        .method(Method::POST)
        .header(header::AUTHORIZATION, auth_header_value)
        .uri(url)
        .body(Body::from(str_body))?;

    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);
    let res: Response<Body> = client.request(req).await?;
    Ok(res)
}

async fn get(uri: &Uri, cookie: Cookies) -> Result<Response<Body>> {
    let url: String = format!("{}{}", DB_BASE_URI, uri);

    let session_token: String = cookie
        .get("sessionToken")
        .and_then(|cookie| cookie.value().parse().ok())
        .unwrap();

    let _refresh_token: String = cookie
        .get("sessionRefreshToken")
        .and_then(|cookie| cookie.value().parse().ok())
        .unwrap();

    let auth_header_value = format!("Bearer {}", session_token);

    let req = Request::builder()
        .method(Method::GET)
        .header(header::AUTHORIZATION, auth_header_value)
        .uri(url)
        .body(Body::empty())?;

    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);
    let res: Response<Body> = client.request(req).await?;
    Ok(res)
}

pub async fn create_user(cookie: Cookies, payload: CreateUser) -> Result<Response<Body>> {
    let endpoint = format!("/Users?documentId={}", payload.get_username());
    let uri: Uri = endpoint.parse::<Uri>().expect("error with uri create user");

    // Firestore DB requests have following payload format.
    //{
    //    "fields": {
    //      "name": {
    //        "stringValue": "Bobby"
    //      },
    //      "nage": {
    //        "integerValue": 15
    //      },
    //      <key>: {
    //          <type>: <value>
    //      }
    //    }
    //}
    let mut obj: HashMap<String, HashMap<String, String>> = HashMap::new();
    obj.insert(
        "name".to_string(),
        HashMap::from([(
            "stringValue".to_string(),
            payload.get_username().to_string(),
        )]),
    );
    let body = FirestoreRequestPayload::new(obj);
    //    let firebase_payload = FirestoreRequestPayload::new(obj);

    let res: Response<Body> = post(&uri, body, cookie).await?;

    // Firestore response body
    // {
    //     "name": "projects/react-rust-boilerplate/databases/(default)/documents/Users/Bobby",
    //     "fields": {
    //         "name": {
    //             "stringValue": "Bobby"
    //         }
    //     },
    //     "createTime": "2022-06-23T01:49:21.926666Z",
    //     "updateTime": "2022-06-23T01:49:21.926666Z"
    // }

    Ok(res)
}
