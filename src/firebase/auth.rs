use hyper::{Body, Client, Method, Request, Response, Uri};
use hyper_tls::HttpsConnector;
use serde_json;

use crate::firebase::config::FIREBASE_API_KEY;
use crate::firebase::errors::FirebaseError;
use crate::models::request::{
    DeleteAccountPayload, RefreshTokenPayload, SignInAnonPayload, SignInPayload,
};

type Result<T> = std::result::Result<T, FirebaseError>;

static FIREBASE_AUTH_URI: &str = "https://identitytoolkit.googleapis.com/v1";
static FIREBASE_TOKEN_URI: &str = "https://securetoken.googleapis.com/v1";

async fn post<T>(uri: &Uri, body: T) -> Result<Response<Body>>
where
    T: serde::Serialize,
{
    let str_body: String = serde_json::to_string(&body)?;
    let req = Request::builder()
        .method(Method::POST)
        .uri(uri)
        .body(Body::from(str_body))?;

    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);
    let res: Response<Body> = client.request(req).await?;
    Ok(res)
}

pub async fn create_user(body: &SignInPayload) -> Result<Response<Body>> {
    let uri = format!(
        "{FIREBASE_AUTH_URI}/accounts:signUp?key={FIREBASE_API_KEY}",
        FIREBASE_API_KEY = FIREBASE_API_KEY,
        FIREBASE_AUTH_URI = FIREBASE_AUTH_URI
    )
    .parse::<Uri>()
    .expect("Failed to parse URI!");

    let res: Response<Body> = post(&uri, body).await?;
    Ok(res)
}

pub async fn sign_in_user(body: &SignInPayload) -> Result<Response<Body>> {
    let uri = format!(
        "{FIREBASE_AUTH_URI}/accounts:signInWithPassword?key={FIREBASE_API_KEY}",
        FIREBASE_API_KEY = FIREBASE_API_KEY,
        FIREBASE_AUTH_URI = FIREBASE_AUTH_URI
    )
    .parse::<Uri>()
    .expect("Failed to parse URI!");

    let res: Response<Body> = post(&uri, body).await?;
    Ok(res)
}

pub async fn sign_in_anon(body: &SignInAnonPayload) -> Result<Response<Body>> {
    let uri = format!(
        "{FIREBASE_AUTH_URI}/accounts:signUp?key={FIREBASE_API_KEY}",
        FIREBASE_API_KEY = FIREBASE_API_KEY,
        FIREBASE_AUTH_URI = FIREBASE_AUTH_URI
    )
    .parse::<Uri>()
    .expect("Failed to parse URI!");

    let res: Response<Body> = post(&uri, body).await?;
    Ok(res)
}

pub async fn delete_user(body: &DeleteAccountPayload) -> Result<Response<Body>> {
    let uri = format!(
        "{FIREBASE_AUTH_URI}/accounts:delete?key={FIREBASE_API_KEY}",
        FIREBASE_API_KEY = FIREBASE_API_KEY,
        FIREBASE_AUTH_URI = FIREBASE_AUTH_URI
    )
    .parse::<Uri>()
    .expect("Failed to parse URI!");

    let res: Response<Body> = post(&uri, body).await?;
    Ok(res)
}

pub async fn refresh_token(body: &RefreshTokenPayload) -> Result<Response<Body>> {
    let uri = format!(
        "{FIREBASE_TOKEN_URI}/token?key={FIREBASE_API_KEY}",
        FIREBASE_API_KEY = FIREBASE_API_KEY,
        FIREBASE_TOKEN_URI = FIREBASE_TOKEN_URI
    )
    .parse::<Uri>()
    .expect("Failed to parse URI!");

    let res: Response<Body> = post(&uri, body).await?;
    Ok(res)
}
