use std::env;

pub static FIREBASE_API_KEY: &'static str = env!("FIREBASE_API_KEY");
