use axum::{
    http,
    response::{IntoResponse, Response},
};
use hyper::StatusCode;
use std::error;
use std::fmt;

#[derive(Debug)]
pub struct FirebaseError {
    status_code: StatusCode,
    message: String,
}

impl FirebaseError {
    pub fn new(code: StatusCode, message: String) -> FirebaseError {
        FirebaseError {
            status_code: code,
            message: message,
        }
    }
}

impl fmt::Display for FirebaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Status Code: {}\nMessage:{}",
            self.status_code, self.message
        )
    }
}

impl error::Error for FirebaseError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl From<http::Error> for FirebaseError {
    fn from(err: http::Error) -> Self {
        FirebaseError::new(StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
    }
}
impl From<hyper::Error> for FirebaseError {
    fn from(err: hyper::Error) -> Self {
        FirebaseError::new(StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
    }
}

impl From<serde_json::Error> for FirebaseError {
    fn from(err: serde_json::Error) -> Self {
        FirebaseError::new(StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
    }
}

impl IntoResponse for FirebaseError {
    fn into_response(self) -> Response {
        (
            self.status_code,
            axum::Json(serde_json::json!({
               "error": self.message
            })),
        )
            .into_response()
    }
}

//type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync + FirebaseError>>;
